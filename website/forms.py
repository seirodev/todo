from django import forms
from django.forms.widgets import DateInput
from .models import List

class ListForm(forms.ModelForm):
    class Meta:
        model = List
        fields = [
            'item',
            'completed',
        ]