from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('delete/<list_id>', views.delete, name='delete'),
    path('complete/<list_id>', views.complete, name='complete'),
    path('undo/<list_id>', views.undo, name='undo'),
    path('edit/<list_id>', views.edit, name='edit'),
] 
