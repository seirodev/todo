from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib import messages
from .models import List
from .forms import ListForm
import datetime

# Create your views here.
def index(request):
    now = datetime.datetime.now()
    if request.method == 'POST':
        add_form = ListForm(request.POST or None)
        if add_form.is_valid():
            add_form.save()
            all_items = List.objects.all
            messages.success(request, 'Item Has Been Added', extra_tags="is-success")
            context = {
                'all_items': all_items,
            }
            return render(request, 'website/index.html', context)
    else:
        all_items = List.objects.all
        context = {
            'all_items': all_items,
            'now': now,
        }
        return render(request, 'website/index.html', context)

def delete(request, list_id):
    item = List.objects.get(pk=list_id)
    item.delete()
    messages.success(request, 'Item Has Been Deleted', extra_tags="is-success")
    return redirect('index')

def complete(request, list_id):
    item = List.objects.get(pk=list_id)
    item.completed = True
    item.save()
    return redirect('index')

def undo(request, list_id):
    item = List.objects.get(pk=list_id)
    item.completed = False
    item.save()
    return redirect('index')

def edit(request, list_id):
    if request.method == 'POST':
        item = List.objects.get(pk=list_id)
        edit_form = ListForm(request.POST or None, instance=item)
        if edit_form.is_valid():
            edit_form.save()
            messages.success(request, 'Item Has Been Edited', extra_tags="is-success")
            return redirect('index')
    else:
        item = List.objects.get(pk=list_id)
        context = {
            'item': item,
        }
        return render(request, 'website/edit.html', context)