notificationMessage = document.querySelector('.message-info');

function dismiss() {
    notificationMessage.classList.add('is-hidden');
}

addTaskModal = document.querySelector('.add-task-modal');
openAddModalButton = document.querySelector('.add-task-modal-open');
closeAddModalButton = document.querySelector('.add-task-modal-close');

openAddModalButton.addEventListener('click', function() {
    addTaskModal.classList.add('is-active');
})

closeAddModalButton.addEventListener('click', function() {
    addTaskModal.classList.remove('is-active');
})

appBackground = document.querySelector('.app-bg');

document.addEventListener('DOMContentLoaded', function () {
    var checkbox = document.querySelector('.switch[type="checkbox"]');

    checkbox.addEventListener('change', function () {
        if (checkbox.checked) {
        appBackground.classList.add('is-black');
        console.log('Checked');
        } else {
        appBackground.classList.remove('is-black');
        console.log('Not checked');
        }
    });
});